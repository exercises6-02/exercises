class CreateBarangays < ActiveRecord::Migration[5.2]
  def change
    create_table :barangays do |t|
      t.string :list
      t.text :populations

      t.timestamps
    end
  end
end
