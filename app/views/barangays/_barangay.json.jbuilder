json.extract! barangay, :id, :list, :populations, :created_at, :updated_at
json.url barangay_url(barangay, format: :json)
